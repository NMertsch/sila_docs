# SiLA Documentation
Demo of how a SiLA documentation could look like.
Built with mkdocs-material, but any other Markdown-based static site generator
would work fine.

See [here](https://nmertsch.gitlab.io/sila_docs).
