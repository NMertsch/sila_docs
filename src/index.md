# SiLA - Standardization in Lab Automation

SiLA’s mission is to establish international standards which create open
connectivity in lab automation. SiLA’s vision is to create interoperability,
flexibility and resource optimization for laboratory instruments integration and
software services based on standardized communication protocols and content
specifications. SiLA promotes open standards to allow integration and exchange
of intelligent systems in a cost-effective way.

## Resources

- [SiLA Website](https://sila-standard.com)
- [SiLA Documentation](./docs/index.md)
- [SiLA Specification](./specification.md)
- [SiLA GitLab Group](https://gitlab.com/SiLA2)

## Contact

- [Slack Community](https://sila-standard.com/slack)
- [Contact form](https://sila-standard.com/contact-us/)
