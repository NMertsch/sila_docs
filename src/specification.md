# SiLA 2 specification

The SiLA 2 specification is a multipart specification:

- Part (A) - Overview, Concepts and Core Specification
  - [Release 1.1](https://drive.google.com/file/d/1CP_MTFmGZ89kkSfIXtPNQly97U1rcnuP/view?usp=sharing)
  - [Working Draft](https://docs.google.com/document/d/1nGGEwbx45ZpKeKYH18VnNysREbr1EXH6FqlCo03yASM)
- Part (B) - Mapping Specification
  - [Release 1.1](https://drive.google.com/file/d/1mFUrCgUib0xaqOpa9UGNvbpdq9R9kIHe/view?usp=sharing)
  - [Working Draft](http://drive.google.com/open?id=1-shgqdYW4sgYIb5vWZ8xTwCUO_bqE13oBEX8rYY_SJA)
- Part (C) - Features Index
  - [Release 1.1](https://drive.google.com/file/d/1gzWd3aMpWyQTkbaVpOsQD1MeYmylYl6w/view?usp=sharing)
  - [Working Draft](http://drive.google.com/open?id=1J9gypD6HofLQZ8cPgLWljRuO0V8l5dS22TWQxFy4bhY)
