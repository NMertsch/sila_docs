# SiLA 2
SiLA 2 is a communication framework for exchanging electronic laboratory
information. It is based on open, well-established communication protocols and
defines a thin domain-specific layer on top of these, consisting of common
concepts and a shared vocabulary.

As the [specification](/specification.md) itself, this documentation is split in
two parts:

- [Concepts](./concepts/index.md)
- [Technical Mapping](./technical-mapping/index.md)
