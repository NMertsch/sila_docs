# Generate Server Certificates
SiLA 2 specifies that all communication between SiLA Servers and SiLA Clients
must be TLS-encrypted.
This means that a server must use a valid SSL certificate, and that a client
should only communicate with trustworthy certificates.

## Who should provide the server certificate?
### Public servers (internet)
For publicly available SiLA Servers, it is highly recommended to use a
certificate signed by publicly trusted certificate authority like Let's Encrypt.
Most SiLA Clients will trust them without any manual effort.

### Non-public servers (company/lab network)
For non-public servers, it is recommended that the network administrators
provide the server certificates. The certificate used for signing these server
certificates should be installed on the client systems so that trust can be
established.

### Development servers
For development purposes, developers may use encrypted communication via
self-signed certificates.

Unencrypted communication violates the SiLA specification, but is an easy way
to test other parts of the SiLA communication.
Please note that some SiLA client applications might not allow unencrypted
communication.

## How to generate a certificate

[//]: # (TODO)

### Self-signed certificates

[//]: # (TODO)
