# Server-Client Architecture
There are two roles in the SiLA 2 communication: A SiLA Server and a SiLA
Client.

## SiLA Server
A SiLA Server offers services to SiLA Clients. Typical representatives are
laboratory devices or databases.

For more information about SiLA Servers, see [here](./server-detailed.md).

## SiLA Client
A SiLA Client is connected to a SiLA Server and consumes its
services ([SiLA Features](./features.md)). Typical representatives are a device
control software like a LIMS (laboratory information management system) or an LES 
(laboratory execution system).

![Example of SiLA Server/Client communication: A LIMS acts as a SiLA Client to interact with a balance and a CDS](./img/server-client.png)

## Multi-role SiLA systems
SiLA Server and SiLA Client are the two roles in SiLA communication, but they
don't have to be separate entities. One system can act as both SiLA Server and
SiLA Client.

One example can be a LIMS system:

- Acts as SiLA Client to control lab devices
- Acts as SiLA Server to provide access to its database

![Illustration of multi-role SiLA communication: A: lab devices, B:LIMS](./img/multi-role-system.png)
