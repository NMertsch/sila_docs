# Property
A property describes an aspect of a SiLA Server which can be read by SiLA 
Clients.

Each property consists of a name and a [Data Type](./data-types.md).

Fridge example:

- Property "Door Is Open", type [Boolean](./data-types.md#Boolean)
- Property "Current Temperature", type [Real](./data-types.md#Real) with a
  [Unit Constraint](./constraints.md#Unit) (e.g. Celsius)

## Unobservable Properties

For unobservable properties, the SiLA Client has to explicitly read the current 
value. The SiLA Server will not notify the client when the property value 
has changed.

Unobservable properties are recommended for constant values, like the serial 
number of a device.

## Observable Properties

With observable properties, the SiLA Client can read the current value (like 
with unobservable properties), but it can also subscribe to changes.
When a property value changes on the SiLA Server, the server sends the new 
value to all subscribed SiLA Clients.

## Execution Errors

A SiLA Server may issue an [Execution Error](./errors.md#Execution-Error) 
when a SiLA Client requests property values and the server is not able to 
determine the property value.
