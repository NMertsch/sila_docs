# Advanced Scenarios
## SiLA Devices
SiLA Devices are SiLA Servers which are physical pieces or mechanical or 
electronic equipment. SiLA devices must support automatic connection via
[SiLA Server Discovery](./discovery.md).

## Multi-client systems
The SiLA 2 specification requires that a SiLA Server always allows at least one
connection from a SiLA Client. Most SiLA Servers do not have an explicit limit,
but may only allow one SiLA Client at a time to access a subset of its
functionality.

## Concurrent command execution
When a command execution is requested while the SiLA Server is busy with
executing the same or another command, it can choose between three options:

- Parallel execution: Execute the requested command in parallel to already
  running command executions
- Queueing: Execute the requested command at a later point in time
- Rejection: Reject the command execution by issueing
  the [Command Execution Not Accepted](./errors.md#Command-Execution-Not-Accepted)
  error

## Multiple feature versions
A SiLA Server may implement multiple versions of the same [feature](./features.md).

## Size of SiLA Client Metadata
[Metadata](./metadata.md) is intended for small pieces of data. The transmission
might fail for values larger than 1 KB.

## Comparison of Fully Qualified Identifiers
Fully Qualified Identifiers must be compared in a case-insensitive way.

## Empty/Void type
SiLA has no built-in "empty" data type. To declare than an [Any](./data-types.md#Any) contains no value,
use a [String](./data-types.md#String) type, constrained to have the [Length](./constraints.md#Length) 0.
