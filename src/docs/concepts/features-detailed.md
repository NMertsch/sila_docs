
## Feature details
Each feature has to provide the following information about itself:

- Display Name: Human-readable name
  - Example: "Calibration Service"
- Description: Human-readable description
  - Example: "This feature provides means to calibrate the device"
- Identifier: Machine-readable name
  - Must start with a capital letter, cannot contain spaces or special
    characters
  - Usually the Display Name without spaces
  - Example: "CalibrationService"
- Originator: The organization authoring the feature
  - In [reverse domain name notation](https://en.wikipedia.org/wiki/Reverse_domain_name_notation)
  - Example: "org.silastandard" (original domain name: "sila-standard.org")
- Category: Feature category
  - In lowercase letters, subcategories separated by dots
  - Examples: "core", "robotics.arm"
  - Default: "none"
- Feature version: Version of the feature
  - Major and minor version number, separated by a dot
  - Examples: "1.0", "0.1"
- SiLA 2 version: Version of SiLA 2
  - Use the current version of the SiLA 2 specification: "1.1"
- Maturity level: One of the following
  - "Draft": Feature is in development
  - "Verified": Verified to meet the SiLA 2 specification and follow best practices
  - "Normative": (only applicable for originator `org.silastandard`) Officially released SiLA 2 standard feature
