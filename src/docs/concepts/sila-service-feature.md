# SiLA Service Feature
The [SiLA Service Feature](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/core/SiLAService-v1_0.sila.xml)
must be implemented by every SiLA Server.
The feature provides information about the server, including its implemented [features](./features.md).

[//]: # (TODO)
