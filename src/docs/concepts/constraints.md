# Constraints
A [Data Type](./data-types.md) can be annotated with constraints. These can be
purely informational (e.g. [Unit](#Unit), [Content Type](#Content-Type)), or
constrain the value space of a  type (e.g. [Length](#Length) or [Set](#Set)).

## Length
Specifies the exact length of a [String](./data-types.md#String) or 
[Binary](./data-types.md#Binary).

## Maximal Length
Specifies the maximum length of a [String](./data-types.md#String) or 
[Binary](./data-types.md#Binary).

## Minimal Length
Specifies the minimum length of a [String](./data-types.md#String) or 
[Binary](./data-types.md#Binary).

## Maximal Inclusive
Specifies the inclusive upper bound of a [Integer](./data-types.md#Integer),
[Real](./data-types.md#Real),
[Date](./data-types.md#Date),
[Time](./data-types.md#Time), or
[Timestamp](./data-types.md#Timestamp).

## Maximal Exclusive
Specifies the exclusive upper bound of a [Integer](./data-types.md#Integer),
[Real](./data-types.md#Real),
[Date](./data-types.md#Date),
[Time](./data-types.md#Time), or
[Timestamp](./data-types.md#Timestamp).

## Minimal Inclusive
Specifies the inclusive lower bound of a [Integer](./data-types.md#Integer),
[Real](./data-types.md#Real),
[Date](./data-types.md#Date),
[Time](./data-types.md#Time), or
[Timestamp](./data-types.md#Timestamp).

## Minimal Exclusive
Specifies the exclusive lower bound of an [Integer](./data-types.md#Integer),
[Real](./data-types.md#Real),
[Date](./data-types.md#Date),
[Time](./data-types.md#Time), or
[Timestamp](./data-types.md#Timestamp).

## Set
Specifies the allowed set of values of a [String](./data-types.md#String),
[Integer](./data-types.md#Integer),
[Real](./data-types.md#Real),
[Date](./data-types.md#Date),
[Time](./data-types.md#Time), or
[Timestamp](./data-types.md#Timestamp).

## Pattern
Specifies a [Regular Expression](https://en.wikipedia.org/wiki/Regular_expression) ("RegEx")
which must be satisfied by a [String](./data-types.md#String).
The RegEx must follow the [XML Schema Dialect](https://www.w3.org/TR/xmlschema11-2/#regexs).

## Unit
Specified the unit of an [Integer](./data-types.md#Integer) or [Real](./data-types.md#Real).
A unit consists of a label (e.g. "hour") and its conversion to SI units (e.g. "3600 seconds").

## Content Type
Specifies the [RFC 2045 content type](https://www.ietf.org/rfc/rfc2045.txt) of a
[String](./data-types.md#String) or [Binary](./data-types.md#Binary) (e.g. `application/xml` for XML data).

## Schema
Specifies that a [String](./data-types.md#String) or [Binary](./data-types.md#Binary) follows a given 
[XML Schema](https://www.w3.org/XML/Schema) or 
[JSON Schema](https://json-schema.org/specification.html).

## Fully Qualified Identifier
Specifies that a [String](./data-types.md#String) must be the
[fully qualified identifier](/docs/fdl/fully-qualified-identifiers.md)
of a
[Feature](./features.md), 
[Command](./command.md), 
[Command Parameter](./command.md#Parameters-and-Responses),
[Command Response](./command.md#Parameters-and-Responses),
[Intermediate Command Response](./command.md#Intermediate-Responses),
[Property](./property.md),
[Custom Data Type](./data-types.md#Custom-Data-Type),
[Metadata](./metadata.md), or
[Defined Execution Error](./errors.md#Defined-Execution-Error).

## Allowed Types
Specifies the [Data Types](./data-types.md) which an [Any](./data-types.md#Any)
can represent.

## Element Count
Specifies the exact number of elements in a [List](./data-types.md#List).

## Minimal Element Count
Specifies the minimal number of elements in a [List](./data-types.md#List).

## Maximal Element Count
Specifies the maximal number of elements in a [List](./data-types.md#List).
