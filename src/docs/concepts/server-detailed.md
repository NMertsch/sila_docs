# SiLA Server
## Server Properties
Each SiLA Server has the following properties:

- Server Name: Human-readable name for the server
  - May change while the server is online
  - Max. 255 characters
  - Example: "Balance (Room 0.12)"
- Server Type: Type of the server
  - Starts with an uppercase letter, cannot contain spaces
  - Example: "Balance"
- Server Description: Long-form description of the server
- Server Vendor URL: Website of the vendor or product
- Server [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier):
  Unique server identifier
  - Example: `584fb5d7-a1fc-4d81-a4d1-079d2c6aa261`
- Server Version
  - Required: Major and minor version number
  - Optional: Patch version and text 
  - Examples: "1.0", "1.2.3", "1.3.0_beta1"
