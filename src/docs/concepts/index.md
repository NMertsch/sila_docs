# SiLA Concepts
This part of the documentation describes SiLA Concepts, as specified in Part A
of the SiLA specification.

If you are new to SiLA, it is recommended to start by reading the following
high-level sections:

- [SiLA Servers and Clients](./server-client.md)
- [SiLA Features](./features.md)
