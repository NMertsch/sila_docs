# Command
A command represents an action that can be performed by a SiLA Server.
Commands may require parameters and may respond by sending data back to the 
SiLA Client.

## Parameters and Responses
A command can have multiple parameters and responses. Each parameter and 
response has a name and a [Data Type](./data-types.md).

There are no optional parameters and no default values. If a command has 
parameters, the SiLA Client has to send values for all of them.

Fridge example:

- Command "Open Door" requires no parameters and returns no data
- Command "Set Temperature" requires a parameter: The target temperature

Camera example:

- Command "Take Picture" requires no parameters and returns data: The picture

## Command Execution
A SiLA Client can request a SiLA Server to execute a command. To do that, the
client has to send all parameters. There are three phases of each command
execution:

1. Parameter Validation: The SiLA Server validates that all parameter values 
   were received and that all received parameter values are valid.
   See also [Validation Error](./errors.md#Validation-Error)).
2. Execution: The SiLA Server performs all actions required for completing 
   the command. See also [Execution Error](./errors.md#Execution-Error).
3. Completion: The SiLA Server completed all actions, notifies the SiLA Client
   and provides the command responses (if any).

## Observability
Commands can either be observable or unobservable.

## Unobservable Commands
These follow a simple execution schema: The client sends a request to the server
and only receives a response once the server completed the command execution.

Unobservable commands offer no way to for the client to observe the progress of
the command execution. Thus, it is best suited for commands with a short 
duration.

## Observable Commands
If a command is observable, the SiLA Server can send progress updates, status 
updates, and intermediate responses to the SiLA Client during command execution.

The execution schema is more complex:

1. The SiLA Client sends the request and all parameter values.
2. The SiLA Server responds with an execution ID.
3. The SiLA Client can use this execution ID to subscribe to status updates 
   and intermediate responses for the executed command.
4. Once the SiLA Server completed the command execution, the SiLA Client can 
   use the execution ID to request the command responses.

Observable commands are especially useful for long-running commands.

### Lifetime of the Execution ID
The SiLA Server can constrain the lifetime of the execution ID. After the 
end of its lifetime, the execution ID can no longer be used. See also
[Invalid Command Execution UUID](./errors.md#Invalid-Command-Execution-UUID).

It is strongly recommended that a SiLA Server constrains the lifetime of the 
execution ID.

### Status Updates
A SiLA Client can use the execution ID to subscribe to status updates of a 
command execution. A status update includes the following information:

- Status: One of "waiting", "running", "finished with error", "finished 
  successfully"
- Progress (optional): The progress of the command execution as a percentage 
  between 0 % and 100 %.
- Estimated remaining time (optional): The expected time until command 
  completion
- Updated lifetime of execution (optional): The new lifetime of the execution ID
  (must not be shorter than the previous lifetime)

### Intermediate Responses
If an observable command defines intermediate responses, a SiLA Client can use
the execution ID to subscribe to them.
An intermediate response has a name and a [Data Type](./data-types.md).
An observable command can define multiple intermediate responses. If 
multiple intermediate responses are defined, the SiLA Server must send 
values for all of them at once. It is not possible to send values for 
different intermediate responses individually.
