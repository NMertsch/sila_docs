# SiLA Client Metadata
A SiLA Server can require that SiLA Clients send additional data ("Metadata") 
along with requests for reading/subscribing to [properties](./property.md) or 
executing [commands](./command.md).
A typical use case is an authorization token.

SiLA Client Metadata ("Metadata") has to be specified in a 
[feature](./features.md) and consists of a name and a
[data type](./data-types.md).

## Affected commands and properties
The SiLA Server must specify which metadata affects which commands and 
properties, and this information must not change while the server is running.

When a SiLA Client does not send the required metadata along with a request, 
the SiLA Server must raise the 
[InvalidMetadata](./errors.md#Invalid-Metadata) framework error.
When a SiLA Client sends metadata along with a request which does not 
require this metadata, the SiLA Server must ignore it.

The [SiLA Service](./sila-service.md) feature must not be affected by metadata.
When a SiLA Client sends metadata along with a request targeted at commands 
or properties defined in the SiLA Service feature, the SiLA Server must 
raise the [No Metadata Allowed](./errors.md#No-Metadata-Allowed) framework 
error.

## Execution Errors
The usage of metadata can lead to 
[Execution Errors](./errors.md#Execution-Error).

## Example
The [Authorization Service](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/core/AuthorizationService-v1_0.sila.xml)
feature defines the metadata "Access Token". The 
[Defined Execution Error](./errors.md) "Invalid Access Token" can be raised 
when this metadata is used. These are the possible scenarios when reading a 
property:

- A SiLA Client does NOT send an access token when reading an affected 
  property. The SiLA Server raises an 
  [Invalid Metadata](./errors.md#Invalid-Metadata) error.
- A SiLA Client sends an access token when reading a property which is NOT 
  affected. The SiLA Server ignores it.
- A SiLA Client sends an access token when reading an affected property.
  - The access token is correct. The SiLA Server responds with the property
    value.
  - The access token is incorrect. The SiLA Server raises the "Invalid Access 
    Token" [Execution Error](./errors.md#Execution-Error).
  - The access token is correct, but the SiLA Server is not able to determine
    the property value. The SiLA Server raises an
    [Execution Error](./errors.md#Execution-Error).
  - The SiLA Server is not able to verify the access token
    (internal server error). The SiLA Server raises an
    [Execution Error](./errors.md#Execution-Error).
