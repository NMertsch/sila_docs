# SiLA Features
SiLA features describe the capabilities of a SiLA Server.

A SiLA Server may implement multiple features. This can be used to separate the
capabilities of a server into meaningful groups. Example: A fridge could
implement the features "Temperature Controller" and "Door Controller".

## Commands and properties
Commands and properties are the main content of SiLA features. They each
describe a single capability that a SiLA Server offers to SiLA Clients.

Commands are what the server "can do", while properties are what the server
"has".

Fridge example:

- Property: "Current Temperature"
- Property: "Door Is Open"
- Command: "Set Temperature"
- Command: "Open Door"

Robot example:

- Property: "Battery Status"
- Command: "Move To Location"

For more information, see [Command](./command.md) and [Property](./property.md).

## Custom Data Types
SiLA specifies a comprehensive system of data types which can be exchanged
between SiLA Clients and SiLA Servers. It is possible to define complex data
types inside a feature and use them in multiple places.

Example: A robot could define the battery status as a combination of the current
battery percentage and a state like "full", "charging" or "discharging".

See [Data Types](./data-types.md) for more information.

## Metadata
A feature can define a type of metadata which SiLA Clients can send along with
requests to access a property or execute a command defined in any feature.

Example:
The [Authorization Service](https://gitlab.com/SiLA2/sila_base/-/blob/master/feature_definitions/org/silastandard/core/AuthorizationService-v1_0.sila.xml)
feature specifies the metadata "Access Token", which a SiLA Client has to 
send along with every request in order to authorize itself to the SiLA Server.

See [Metadata](./metadata.md) for more information.

## Defined Execution Errors
A feature can define specific [Execution Errors](./errors.md#Execution-Errors).
These can be issued by the SiLA Server during handling of commands, properties
and metadata defined in the same feature. This requires that the defined
execution error is referenced in the definition of the respective command,
property, or metadata.

## Feature Repository
While features can be designed by anyone, SiLA provides a standardized set of
features for common use cases. Any party can publish their features through
the [SiLA feature repository](./feature-repository.md).

## SiLA Service Feature
Every SiLA Server must implement the [SiLA Service Feature](./sila-service-feature.md).
It provides general information about the server.
