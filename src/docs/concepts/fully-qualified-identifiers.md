# Fully Qualified Identifiers
Many components of a [SiLA feature](./features.md) have an identifier, which is the human-readable name of the
component.

To uniquely identify these components, they also have a fully qualified identifier.
Users are rarely required to directly interact with those, but they are frequently used internally in SiLA
communication.

Fully qualified identifiers are composed according to the following rules.

## Feature Identifier
[//]: # (TODO)
