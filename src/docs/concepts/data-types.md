# Data Types
As a communication framework, SiLA's job is to send messages between SiLA 
Servers and SiLA Clients. These messages can contain data, and these pieces 
of data can have different types. SiLA supports basic data types (like text 
or numbers), but also custom types as a combination of these basic types.

Additionally, many data types can be annotated with 
constraints. This enables concepts like "the fridge 
temperature is a real number between -100 and 0 degrees Celsius".
See [Constraints](./constraints.md) for details.
The usage of constraints is strongly recommended.

## String
A SiLA String represents plain text of up to 2x2^20 Unicode characters.
For larger pieces of data, the [Binary](#Binary) type can be used.

Examples:

- "Hello World"
- "This is an error message"
- ""

Applicable constraints:
[Length](./constraints.md#Length),
[Maximal Length](./constraints.md#Maximal-Length),
[Minimal Length](./constraints.md#Minimal-Length)

## Integer
A SiLA Integer number represents an [Integer](https://en.wikipedia.org/wiki/Integer
number between -2^63 (approx. -9.2x10^18) and 2^63-1 (approx. 9.2x10^18).

Examples:

- 1
- 10,000
- -42

## Real
A SiLA Real number represents an 
[IEEE 754 double-precision floating-point number](https://en.wikipedia.org/wiki/Double-precision_floating-point_format).

Examples:

- 1.0
- -10.4
- 273.15

## Boolean
A Sila Boolean represents a
[Boolean](https://en.wikipedia.org/wiki/Boolean_data_type)
value.

Possible values:
- True
- False

## Binary
A SiLA Binary represents arbitrary
[Binary Data](https://en.wikipedia.org/wiki/Binary_file).

Examples:

- PNG-encoded image

Applicable constraints:
[Length](./constraints.md#Length),
[Maximal Length](./constraints.md#Maximal-Length),
[Minimal Length](./constraints.md#Minimal-Length)

## Date
A SiLA Date represents a date (year, month, day) and a timezone.

Examples:

- January 31st 1990, UTC
- February 29th 2004, Pacific Time (UTC-08:00)

## Time
A SiLA Time represents a time (hour, minute, second, optional millisecond)
and a timezone.

Examples:

- 08:00:00
- 12:23:56.789

## Timestamp
A SiLA Timestamp represents a [date](#Date), [time](#Time), and timezone.

## List
A SiLA List is an ordered container for items of the same data type.
The item type of a List cannot be another List. However, it can be a
[Custom Data Type](#Custom-Data-Type) which describes a List.

Examples:

- [1, 2, 5]
- []
- [08:00:00, 09:30:00, 11:00:00]

## Structure
A SiLA Structure is a type composed of one or more named elements.
The structure itself has no name. It might make sense to wrap a structure in a
[Custom Data Type](#Custom-Data-Type).

Example:
The position of a well on a multi-titer plat can be modeled as a structure with
the elements
- Name: "X", type: Integer
- Name: "Y", type: Integer
- Name: "Z", type: Integer

## Custom Data Type
In the context of a [SiLA Feature](./features.md) it is possible to define
custom data types. A custom data type wraps another data type and has a name
and description.

This is especially useful if the same type of data is used in multiple parts of
a feature (e.g. as property and command parameter), or to provide a name to a
complex data type like a structure or a data type with constraints.

## Any
The SiLA Any type can be used for any type of data. Values of the Any type
contain the description of the actual data type.

The Any type cannot contain information modeled as a 
[Custom Data Type](#Custom-Data-Type) and should be avoided if possible.

Examples:

- Type: Integer, Value: 123
- Type: String, Value: "ABC"

## Void
The SiLA Void type represents no data and is only valid as the value of the
[Any](#Any) type.
